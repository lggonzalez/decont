#Download the sequencing data files.
for url in $(cat data/urls)
do
	wget -i -O $url -P data  #Bonus1
done;
echo "Secuencias descargadas.."
# Download the contaminants database, and uncompress it. 
	#Si no se quiere desconprimir sustituir "yes" en el ultimo argumento.
bash scripts/download.sh https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz res yes
echo "Contaminante descargado.."

#Index the contaminants database.
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx
echo "Index contaminante.."

#Merge the fastqs from the same sample into a single file
mkdir -p out/merged
mkdir -p out/star
mkdir -p out/trimmed
for sid in $(ls data/*.fastq.gz | cut -d"-" -f1 | sed "s:data/::" | sort | uniq)
do
    bash scripts/merge_fastqs.sh data out/merged $sid
    echo "Merged.."
done;

#Cutadapt for all merged files
mkdir -p log/cutadapt
for sid in $(ls out/merged | cut -d"." -f1)
do
	echo "Running cutadapt.."
    cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${sid}.trimmed.fastq.gz out/merged/${sid}.fastq.gz > log/cutadapt/${sid}.log
done;

#STAR for all trimmed files
for fname in $(ls out/trimmed/*.fastq.gz | cut -d"." -f1)
do
    echo "Running STAR.."
    basename $fname
    sid=$(basename -- $fname)
    mkdir -p out/star/$sid
    STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn out/trimmed/$sid.trimmed.fastq.gz --readFilesCommand zcat -c --outFileNamePrefix out/star/$sid/
    #Cambie gzip por zcat, porque no funcionaba con gzip
    echo "STAR finalizado.."
done;

#Summary Log
for sid in $(ls out/merged | cut -d"." -f1)
do
	echo $sid >> Log.out
	echo "cutadapt" >> Log.out
	grep "Reads with adapters:" log/cutadapt/$sid.log >> Log.out
	grep "Total basepairs processed:" log/cutadapt/$sid.log >> Log.out
	echo "star" >> Log.out
    grep "Uniquely mapped reads %" out/star/$sid/Log.final.out >> Log.out
	grep "% of reads mapped to multiple loci" out/star/$sid/Log.final.out >> Log.out
	grep "% of reads mapped to too many loci" out/star/$sid/Log.final.out >> Log.out
done;
