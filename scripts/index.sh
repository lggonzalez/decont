# This script should index the genome file specified in the first argument ($1),
# creating the index in a directory specified by the second argument ($2).
if [ "$#" -eq 2 ]
then
	mkdir $2 -p
	STAR --runThreadN 4 --runMode genomeGenerate --genomeDir $2 --genomeFastaFiles $1 --genomeSAindexNbases 9
	echo "Indexado.."
else
	echo "No se pudo indexar.."
    exit 1
fi