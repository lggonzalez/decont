# This script should download the file specified in the first argument ($1), place it in the directory specified in the second argument ($2), 
# and *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes".[$3=="yes"]
if [ "$#" -eq 2 ]
then
    wget -i -O $1 -P $2
elif [ "$#" -eq 3 ]
then
    wget -i -O $1 -P $2
    if [ $3 == "yes" ]
    then
    	cd res
    	gunzip -k contaminants.fasta.gz
    else
    	echo "No descomprimir"
    fi
else
	echo "Usage: $0 <sampleid>"
    exit 1
fi